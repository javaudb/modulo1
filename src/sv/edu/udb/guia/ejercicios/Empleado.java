package sv.edu.udb.guia.ejercicios;

public class Empleado {
    String nombre;
    String sexo;
    int horasTrabajadas;
    double sueldoHora;
    double pagoTotal;

    public Empleado(String nombre, String sexo, int horasTrabajadas,
                    double sueldoHora){
        this.nombre = nombre;
        this.sexo = sexo;
        this.horasTrabajadas = horasTrabajadas;
        this.sueldoHora = sueldoHora;
        this.pagoTotal = this.calcularTotalPagar();
    }
    
    private double calcularTotalPagar(){
        double pagoTotalPagar = this.sueldoHora*this.horasTrabajadas;
        return pagoTotalPagar;
    }

}