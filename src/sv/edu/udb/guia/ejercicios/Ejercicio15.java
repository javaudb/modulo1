package sv.edu.udb.guia.ejercicios;

public class Ejercicio15 {

    public static void main(String[] args) {
        System.out.println("____________________________");        
        System.out.println("    Yardas   |   Pulgadas   ");
        for(int i=1; i<=10; i++){
            if(i<10){
                System.out.println("      "+i+"      |      " + i*36 + "     ");
            }else{
                System.out.println("      "+i+"     |      " + i*36 + "     ");
            
            }
        }
        System.out.println("____________________________");
    }
    
}
