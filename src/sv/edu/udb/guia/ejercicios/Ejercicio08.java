package sv.edu.udb.guia.ejercicios;

import java.util.Scanner;

public class Ejercicio08 {
    
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        double ventas;
        double total;
        
        System.out.println("Bienvenido. Ingrese sus ventas: ");
        ventas = Double.parseDouble(in.nextLine());
        
        if(ventas<200){
            total = 200 + ventas;
        }else if(ventas>200){
            total = 500 + (ventas*0.1);
        }else{
            total = 500;
        }
        System.out.println("Su salario a pagar es $" + total);
    }
}
