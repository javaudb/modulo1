package sv.edu.udb.guia.ejercicios;

import java.util.Scanner;

public class Ejercicio06 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        double precio;
        double DESC1 = 0.15;
        double DESC2 = 0.1;
        
        System.out.println("Bienvenido. Ingrese precio de los zapatos: ");
        precio = Double.parseDouble(in.nextLine());
        
        System.out.println("Precio normal: $" + precio);
        if(precio >= 150){     
            System.out.println("Precio descuento (15%): $" + precio*0.85);
        }else{
            System.out.println("Precio descuento (5%): $" + precio*0.95);
        }
        
    }
    
}
