package sv.edu.udb.guia.ejercicios;

import java.util.Scanner;

public class Ejercicio02 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String nombre;
        
        System.out.println("Bienvenido. Ingrese su nombre");
        nombre = in.nextLine();
        
        System.out.println("Un saludo " + nombre);
    }
    
}
