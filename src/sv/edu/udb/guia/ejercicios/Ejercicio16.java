
package sv.edu.udb.guia.ejercicios;

import java.util.ArrayList;
import java.util.Scanner;

public class Ejercicio16 {
    
    static ArrayList<Empleado> listaEmpleados = new ArrayList();
    static String nombre;
    static Scanner in = new Scanner(System.in);
    static String sexo;
    static int horasTrabajadas;
    static double sueldoHora;
    
    public static void main(String[] args) {    
        registrarEmpleados();
        mostrarEmpleados();
    }
    
    public static void registrarEmpleados(){
        for(int i=0; i<5; i++){
           System.out.println("Ingrese nombre empleado " + (i+1));
           nombre = in.nextLine();
           System.out.println("Ingrese sexo (F/M)");
           sexo = in.nextLine();
           System.out.println("Ingrese horas trabajadas");
           horasTrabajadas = Integer.parseInt(in.nextLine());
           System.out.println("Ingrese sueldo por hora");
           sueldoHora = Double.parseDouble(in.nextLine());
           System.out.println("\n");
           Empleado empleado = new Empleado(nombre, sexo, horasTrabajadas, 
                   sueldoHora);
           listaEmpleados.add(empleado);
       }
    }
    
    public static void mostrarEmpleados(){
        for(Empleado e: listaEmpleados){
            System.out.println("-------------------------------");
            System.out.println("Nombre: " + e.nombre);
            System.out.println("Horas trabajadas" + e.horasTrabajadas);
            System.out.println("Pago por hora $" + e.sueldoHora);
            System.out.println("Pago total $" + e.pagoTotal);
            System.out.println("\n");
        }
    }
    
}