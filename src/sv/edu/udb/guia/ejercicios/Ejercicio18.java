
package sv.edu.udb.guia.ejercicios;

import java.util.ArrayList;
import java.util.Scanner;

public class Ejercicio18 {
    
    static ArrayList<Alumno> listaAlumnos = new ArrayList();
    static Scanner in = new Scanner(System.in);
    static String nombre;
    static double nota1;
    static double nota2;
    static double nota3;
    
    public static void main(String[] args) {
        registrarAlumnos();
        mostrarAlumnos();
        mostrarEstadisticas();
        
    }
    
    public static void registrarAlumnos(){
        for(int i=0; i<10; i++){
           System.out.println("Ingrese nombre alumno " + (i+1));
           nombre = in.nextLine();
           System.out.println("Ingrese nota 1");
           nota1 = Double.parseDouble(in.nextLine());
           System.out.println("Ingrese nota 2");
           nota2 = Double.parseDouble(in.nextLine());
           System.out.println("Ingrese nota 3");
           nota3 = Double.parseDouble(in.nextLine());
           System.out.println("\n");
           Alumno alumno = new Alumno(nombre, nota1, nota2, nota3);
           listaAlumnos.add(alumno);
       }
    }
    
        
    public static void mostrarAlumnos(){
        for(Alumno a: listaAlumnos){
            System.out.println("-------------------------------");
            System.out.println("Nombre: " + a.nombre);
            System.out.println("Nota 1: " + String.format("%.2f",a.nota1));
            System.out.println("Nota 2: " + String.format("%.2f",a.nota2));
            System.out.println("Nota 3: " + String.format("%.2f",a.nota3));
            System.out.println("Promedio: " + String.format("%.2f",a.promedio));
            
            if(a.estatus.equalsIgnoreCase("A")){
                System.out.println("El alumno ha aprobado");
            }else{
                System.out.println("El alumno a reprobado");
            }
            System.out.println("\n");
        }
    }
    
    public static void mostrarEstadisticas(){
        int cantidadAprobados=0;
        int cantidadReprobados=0;
        int cantidadMas80 =0;
        
        for(Alumno a: listaAlumnos){
           if (a.estatus.equalsIgnoreCase("A")){
               cantidadAprobados++;
               if(a.promedio > 80){
                   cantidadMas80++;
               }
           }else{
               cantidadReprobados++;
           }
        }
        
        System.out.println("Alumnos aprobados: " + cantidadAprobados);
        System.out.println("Alumnos reprobados: " + cantidadReprobados);
        System.out.println("Alumnos con más de 80 de promedio: " + cantidadMas80);
    }
    
}
