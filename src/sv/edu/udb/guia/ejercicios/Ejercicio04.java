/*
Elaborar un programa que permita convertir una medida escogiendo las unidades
actuales y el tipo de conversion que desea. Conversiones de centimetro a pulgadas.
de metro a pies, de pies a pulgadas, metros a cm
 */
package sv.edu.udb.guia.ejercicios;
import java.util.Scanner;

public class Ejercicio04 {

    public static void main(String[] args) {
        int opc;
        double medida;
        double resultado;

        Scanner in = new Scanner(System.in);
        System.out.println("Ingrese numero de opción que desea");
        System.out.println("1. Cm a Pulgadas \n2. M a Ft \n3. Ft a Pulgadas \n4. M a Cm");
        opc = in.nextInt();

        System.out.println("Ingrese valor");
        medida = in.nextDouble();

        switch (opc) {
            case 1:
                resultado = medida / 2.54;
                System.out.println("Conversion: " + medida + " cm --> " + resultado + " pulgadas");
                break;
            case 2:
                resultado = medida * 3.281;
                System.out.println("Conversion: " + medida + " M --> " + resultado + " Ft");
                break;
            case 3:
                resultado = medida * 12;
                System.out.println("Conversion: " + medida + " Ft --> " + resultado + " pulgadas");
                break;
            case 4:
                resultado = medida * 100;
                System.out.println("Conversion: " + medida + " M --> " + resultado + " cm");
                break;
            default:
                System.out.println("Opción no valida");
                break;
        }
    }
}