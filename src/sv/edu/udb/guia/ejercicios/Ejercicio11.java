package sv.edu.udb.guia.ejercicios;

import java.util.Scanner;

/**
 *
 * @author aleev
 */
public class Ejercicio11 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int peso;
        double total = 0.25;
        
        System.out.println("Bienvenido. Por favor ingrese "
                + "peso en onzas de la carta");
        peso = Integer.parseInt(in.nextLine());
        if(peso>0){
            peso = peso-1;
            total = total + peso*0.04;
            System.out.println("Costo total $" + total);
        }else{
            System.out.println("Peso no puede ser menor o igual a 0");
        }
    }
}