package sv.edu.udb.guia.ejercicios;

import java.util.Scanner;

public class Ejercicio09 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int opc;
        double valor;
        double resultado;
        
        System.out.println("Bienvenido. Seleccione opción deseada");
        System.out.println("[1] Fahrenheit a Celsius");
        System.out.println("[2] Celsius a Fahrenheit");
        opc = Integer.parseInt(in.nextLine());
        
        System.out.println("Ingrese valor a convertir");
        valor = Double.parseDouble(in.nextLine());
        switch(opc) {
            case 1:
                resultado = valor-32;
                resultado = resultado *5/9;
                System.out.println("Conversión: " + valor + "F --> " + resultado + "C");
                break;
            case 2:
                resultado = valor*9/5;
                resultado = resultado + 32;
                System.out.println("Conversión: " + valor + "C --> " + resultado + "F");
                break;
            default:
                System.out.println("Opción invalida");
                break;
        }
    }
    
}
