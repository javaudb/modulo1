package sv.edu.udb.guia.ejercicios;

import java.util.Scanner;

public class Ejercicio12 {
    static Scanner in = new Scanner(System.in);
    static String categoriaAlumno; //A o N
    static int cantidadAsignaturas;
    
    public static void main(String[] args) {
        capturarDatos();
        double totalPago = calcularTotal();
        imprimirResultado(totalPago);
    }
    
    public static void capturarDatos(){
        System.out.println("Digite la categoria del alumno (A / N)");
        categoriaAlumno = in.nextLine();
        System.out.println("Digite la cantidad de asignaturas");
        cantidadAsignaturas = Integer.parseInt(in.nextLine());
    }
    
    public static double calcularTotal(){
        double totalPago=0.0;
        if (categoriaAlumno.equalsIgnoreCase("A")){
            totalPago = cantidadAsignaturas * 30;
        }else if(categoriaAlumno.equalsIgnoreCase("N")){
            totalPago = cantidadAsignaturas * 50;
        }
        return totalPago;
    }
    
    public static void imprimirResultado(double totalPago){
        System.out.println("La categoria del alumno es: " + categoriaAlumno);
        System.out.println("La cantidad de asignaturas del alumno es: " 
                + cantidadAsignaturas);
        System.out.println("Total a pagar: $" + totalPago);
        
    }
    
}
