package sv.edu.udb.guia.ejercicios;

import java.util.Scanner;

public class Ejercicio07 {

    public static void main(String[] args) {
        
        Scanner in = new Scanner(System.in);
        String nombre;
        int edad;
        String sexo;
        String telefono;
        
        System.out.println("Bienvenido. Ingrese su nombre: ");
        nombre = in.nextLine();
        
        System.out.println("Ingrese su edad");
        edad = Integer.parseInt(in.nextLine());
        
        System.out.println("Ingrese su sexo (M/F)");
        sexo = in.nextLine();
        
        System.out.println("Ingrese teléfono");
        telefono = in.nextLine();
        
        if(sexo.equalsIgnoreCase("m")){
            System.out.println("El Sr." + nombre + " ha sido registrado");
            System.out.println("Edad " + edad);
            System.out.println("Sexo masculino");
            System.out.println("Teléfono " + telefono);
        }else if(sexo.equalsIgnoreCase("f")){
            System.out.println("La Sra." + nombre + " ha sido registrado");
            System.out.println("Edad " + edad);
            System.out.println("Sexo femenino");
            System.out.println("Teléfono " + telefono);
        }else{
            System.out.println("Sexo indefinido");
        }
    }
    
}
