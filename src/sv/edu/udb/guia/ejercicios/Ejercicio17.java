package sv.edu.udb.guia.ejercicios;

import java.util.ArrayList;
import java.util.Scanner;

public class Ejercicio17 {
    
    static ArrayList<EmpleadoEj17> listaEmpleados = new ArrayList();
    static String nombre;
    static ArrayList<EmpleadoEj17> listaEmpleadosMayores55 = new ArrayList();
    static ArrayList<EmpleadoEj17> listaEmpleadosMenores55 = new ArrayList();
    static Scanner in = new Scanner(System.in);
    static int edad;
    static int horasTrabajadas;
    static double sueldoHora;
    
    public static void main(String[] args) {
        registrarEmpleados();
        mostrarEmpleados();
        mostrarTotalesPorEdad();
        System.out.println("\n");
        mostrarEmpleadoMayor55Sueldo();
        mostrarEmpleadoMenor55Sueldo();
    }
    
      public static void registrarEmpleados(){
        for(int i=0; i<10; i++){
           System.out.println("Ingrese nombre empleado " + (i+1));
           nombre = in.nextLine();
           System.out.println("Ingrese edad");
           edad = Integer.parseInt(in.nextLine());
           System.out.println("Ingrese horas trabajadas");
           horasTrabajadas = Integer.parseInt(in.nextLine());
           System.out.println("Ingrese sueldo por hora");
           sueldoHora = Double.parseDouble(in.nextLine());
           System.out.println("\n");
           EmpleadoEj17 empleado =  
                   new EmpleadoEj17(nombre, edad, horasTrabajadas, sueldoHora);
           listaEmpleados.add(empleado);

       }
    }
     
    public static void mostrarTotalesPorEdad(){
        double totalMayores=0;
        double totalMenores=0;
        for(EmpleadoEj17 e: listaEmpleados){
            if (e.edad >= 55){
                totalMayores += e.sueldoNeto;
                listaEmpleadosMayores55.add(e);
            }else{
                totalMenores += e.sueldoNeto;
                listaEmpleadosMenores55.add(e);
            }
        }
        System.out.println("El total a pagar a empleados de al menos 55"
                + " años es $" + String.format("%.2f",totalMayores));
        
        System.out.println("El total a pagar a empleados de menores a 55"
                + " años es $" + String.format("%.2f",totalMenores));
    }
    
    public static void mostrarEmpleados(){
        for(EmpleadoEj17 e: listaEmpleados){
            System.out.println("-------------------------------");
            System.out.println("Nombre: " + e.nombre);
            System.out.println("Edad: " + e.edad);
            System.out.println("Horas trabajadas " 
                    + e.horasTrabajadas);
            System.out.println("Sueldo por hora $" 
                    + e.sueldoHora);
            System.out.println("Sueldo total $" 
                    + String.format("%.2f",e.sueldoTotal));
            System.out.println("Impuestos retenidos $" 
                    + String.format("%.2f",e.impuestos));
            System.out.println("Sueldo neto $" 
                    + String.format("%.2f",e.sueldoNeto));
            System.out.println("\n");
        }
    }
    
   public static void mostrarEmpleadoMayor55Sueldo(){
        boolean esPrimeraIteracion = true;
        EmpleadoEj17 empleado = new EmpleadoEj17();
        
        for(EmpleadoEj17 e: listaEmpleadosMayores55){
            if(esPrimeraIteracion){
                empleado = e;
            }else{
                if(empleado.sueldoTotal < e.sueldoTotal){
                    empleado=e;
                }
            }
            esPrimeraIteracion = false;
        }
        System.out.println("El empleado de al menos 55 años con el mayor"
                + " sueldo es " + empleado.nombre + " con $" 
                + String.format("%.2f",empleado.sueldoNeto));
  
   }
   
    public static void mostrarEmpleadoMenor55Sueldo(){
        boolean esPrimeraIteracion = true;
        EmpleadoEj17 empleado = new EmpleadoEj17();
        
        for(EmpleadoEj17 e: listaEmpleadosMenores55){
            if(esPrimeraIteracion){
                empleado = e;
            }else{
                if(empleado.sueldoTotal < e.sueldoTotal){
                    empleado=e;
                }
            }
            esPrimeraIteracion = false;
        }
        System.out.println("El empleado de menos de 55 años con el mayor"
                + " sueldo es " + empleado.nombre + " con $" 
                + String.format("%.2f",empleado.sueldoNeto));
    
   }
}