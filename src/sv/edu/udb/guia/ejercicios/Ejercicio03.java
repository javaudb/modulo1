package sv.edu.udb.guia.ejercicios;

import java.util.Scanner;

public class Ejercicio03 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String palabra;
        
        System.out.println("Bienvenido. Ingrese una palabra");
        palabra = in.nextLine();
        
        System.out.println(palabra + " " + palabra + " " + palabra);
    }
    
}
