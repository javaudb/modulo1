
package sv.edu.udb.guia.ejercicios;

import java.util.Scanner;


public class Ejercicio05 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        double precio;
        
        System.out.println("Bienvenido. Ingrese precio de los zapatos: ");
        precio = Double.parseDouble(in.nextLine());
        
        System.out.println("Precio normal: $" + precio);
        System.out.println("Precio descuento: $" + precio*(0.85));
    }
    
}
