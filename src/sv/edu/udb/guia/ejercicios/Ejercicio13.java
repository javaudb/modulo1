
package sv.edu.udb.guia.ejercicios;

import java.util.Scanner;

public class Ejercicio13 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int numero1;
        int numero2;
        int diferencia;
        
        System.out.println("Ingrese numero entero positivo:");
        numero1 = Integer.parseInt(in.nextLine());
        
        System.out.println("Ingrese numero entero positivo diferente:");
        numero2 = Integer.parseInt(in.nextLine());
        
        if (numero1 != numero2) {
            diferencia = Math.abs(numero1) - Math.abs(numero2);
            System.out.println("La diferencia entre ambos es: " + Math.abs(diferencia));
        }else{
            System.out.println("Los numeros ingresados deben ser diferentes");
        }
    }
    
}
