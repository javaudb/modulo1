package sv.edu.udb.guia.ejercicios;
import java.util.Scanner;

public class Ejercicio14 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        
        int cantidadHoras;
        double sueldoHora;
        double sueldoTotal;
        
        System.out.println("Bienvenido. Ingrese cantidad de horas trabajadas");
        cantidadHoras = Integer.parseInt(in.nextLine());
        
        System.out.println("Ingrese sueldo por hora");
        sueldoHora = Double.parseDouble(in.nextLine());
        
        if (cantidadHoras <= 30){
            sueldoTotal = cantidadHoras * sueldoHora;
        }else{
            sueldoTotal = 30*sueldoHora;
            cantidadHoras -= 30;
            sueldoTotal += cantidadHoras*(sueldoHora*1.5);
        }
        System.out.println("sueldoTotal: $" + sueldoTotal);
        
        
    }
    
}
