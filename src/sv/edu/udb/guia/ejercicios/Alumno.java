package sv.edu.udb.guia.ejercicios;

public class Alumno {
    String nombre;
    double nota1;
    double nota2;
    double nota3;
    double promedio;
    String estatus;

    public Alumno(String nombre, double nota1, double nota2, double nota3) {
        this.nombre = nombre;
        this.nota1 = nota1;
        this.nota2 = nota2;
        this.nota3 = nota3;
        this.promedio = calcularPromedio(); 
        if(this.promedio < 60){
            this.estatus = "R";
        }else{
            this.estatus = "A";
        }
    }
    
    private double calcularPromedio(){
        double prom = (this.nota1 + this.nota2 + this.nota3) / 3;
        return prom;
    } 
}
