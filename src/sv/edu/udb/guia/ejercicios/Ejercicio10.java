package sv.edu.udb.guia.ejercicios;

import java.util.Scanner;

/**
 *
 * @author aleev
 */
public class Ejercicio10 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int numero1;
        int numero2;
        
        System.out.println("Bienvenido.");
        System.out.println("Ingrese numero 1:");
        numero1 = Integer.parseInt(in.nextLine());
        System.out.println("Ingrese numero 2:");
        numero2 = Integer.parseInt(in.nextLine());
        
        if ( (numero1>0 && numero2<0) || (numero1<0 && numero2>0) ){
            System.out.println("Son números con signos diferentes");
        }else{
            System.out.println("Son números con mismos signos");
        }
        
    }
    
}
