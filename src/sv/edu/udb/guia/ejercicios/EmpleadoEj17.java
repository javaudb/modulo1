
package sv.edu.udb.guia.ejercicios;


public class EmpleadoEj17 {
        String nombre;
        int edad;
        int horasTrabajadas;
        double sueldoHora;
        double sueldoTotal;
        double impuestos;
        double sueldoNeto;

    public EmpleadoEj17() {

    }

    public EmpleadoEj17(String nombre, int edad, int horasTrabajadas, 
            double sueldoHora) {
        this.nombre = nombre;
        this.edad = edad;
        this.horasTrabajadas = horasTrabajadas;
        this.sueldoHora = sueldoHora;
        this.sueldoTotal = this.calcularSueldo();
        this.impuestos = this.calcularImpuestos();
        this.sueldoNeto = this.sueldoTotal - this.impuestos; 
    }
        
    private double calcularSueldo(){
        double sueldo;
        if (this.horasTrabajadas <= 40){
            sueldo  = this.sueldoHora*this.horasTrabajadas;
        }else{
            sueldo = 40*sueldoHora;
            int horas = this.horasTrabajadas - 40;
            sueldo += horas*(sueldoHora*1.5);
        }
        return sueldo;
    }
    
    private double calcularImpuestos(){
        double impuestosTotal;
        if(this.sueldoTotal <= 200){
            impuestosTotal = this.sueldoTotal *0.1;
        }else{
            impuestosTotal = 20;
            double sueldoRestante = this.sueldoTotal - 200;
            impuestosTotal += (sueldoRestante*0.2);
        }
        return impuestosTotal;
    }
        
}
